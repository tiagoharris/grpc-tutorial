package builder

import (
	"time"

	proto "bitbucket.org/tiagoharris/grpc-tutorial/gen/pb-go"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// BuildMeteoriteLandingList builds a sample *proto.MeteoriteLandingList
func BuildMeteoriteLandingList() *proto.MeteoriteLandingList {
	var mll proto.MeteoriteLandingList
	mll.MeteoriteLandings = append(mll.MeteoriteLandings,
		&proto.MeteoriteLanding{
			Id:   "6",
			Name: "Abee",
			Fall: "Fell",
			Geolocation: &proto.GeoLocation{
				Type:        "Point",
				Coordinates: []float64{-113, 54.21667},
			},
			Mass:     "107000",
			Nametype: "Valid",
			Recclass: "EH4",
			Reclat:   "54.216670",
			Reclong:  "-113.000000",
			Year:     timestamppb.New(time.Date(1952, time.Month(1), 1, 0, 0, 0, 0, time.UTC)),
		},
		&proto.MeteoriteLanding{
			Id:   "10",
			Name: "Acapulco",
			Fall: "Fell",
			Geolocation: &proto.GeoLocation{
				Type:        "Point",
				Coordinates: []float64{-99.9, 16.88333},
			},
			Mass:     "1914",
			Nametype: "Valid",
			Recclass: "Acapulcoite",
			Reclat:   "16.883330",
			Reclong:  "-99.900000",
			Year:     timestamppb.New(time.Date(1976, time.Month(1), 1, 0, 0, 0, 0, time.UTC)),
		},
	)
	return &mll
}

// BuildCountResponse builds a *proto.CountResponse that contains
// the count of *proto.MeteoriteLandingList
func BuildCountResponse(nml *proto.MeteoriteLandingList) *proto.CountResponse {
	return &proto.CountResponse{Count: int32(len(nml.MeteoriteLandings))}
}
