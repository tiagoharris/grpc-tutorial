module bitbucket.org/tiagoharris/grpc-tutorial

go 1.16

require (
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.27.1

)
