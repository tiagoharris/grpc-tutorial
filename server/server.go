package server

import (
	"context"
	"fmt"
	"net"

	proto "bitbucket.org/tiagoharris/grpc-tutorial/gen/pb-go"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

// Server defines the functions that GRPC server must implement.
type Server interface {
	Run() error
	Close()
}

// GRPCServer implements Server interface and CountLandings grpc service.
type GRPCServer struct {
	addr     string
	listener net.Listener
	server   *grpc.Server
}

// NewServer returns a pointer to GRPCServer.
// Add is the desired port, certificatePath is the path to the certificate file ('.ctr') and
// privateKeyPath is the path to the private key file ('.key').
func NewServer(addr, certificatePath, privateKeyPath string) (Server, error) {
	creds, err := credentials.NewServerTLSFromFile(certificatePath, privateKeyPath)
	if err != nil {
		return nil, errors.Wrap(err, "loading tls keys")
	}
	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", addr))
	if err != nil {
		return nil, errors.Wrap(err, "tcp listening")
	}
	opts := []grpc.ServerOption{grpc.Creds(creds)}
	srv := grpc.NewServer(opts...)
	grpcServer := &GRPCServer{
		addr:     addr,
		listener: listener,
		server:   srv,
	}
	proto.RegisterProtobufServiceServer(grpcServer.server, grpcServer)
	reflection.Register(grpcServer.server)
	return grpcServer, nil
}

// Run makes the GRPC Server ready to serve requests.
func (g *GRPCServer) Run() (err error) {
	for {
		if err := g.server.Serve(g.listener); err != nil {
			return errors.Wrap(err, "serving requests")
		}
	}
}

// Close shuts down the GRPC Server.
func (g *GRPCServer) Close() {
	g.server.GracefulStop()
}

// CountLandings implements the grcp service with same name.
// It returns the count for the given *proto.MeteoriteLandingList.
func (g *GRPCServer) CountLandings(context context.Context, mll *proto.MeteoriteLandingList) (*proto.CountResponse, error) {
	return &proto.CountResponse{
		Count: int32(len(mll.MeteoriteLandings)),
	}, nil
}
