package server

import (
	"context"
	"log"
	"net"
	"testing"

	"bitbucket.org/tiagoharris/grpc-tutorial/builder"
	proto "bitbucket.org/tiagoharris/grpc-tutorial/gen/pb-go"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	proto.RegisterProtobufServiceServer(s, &GRPCServer{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func TestCountLandings(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()

	mll := builder.BuildMeteoriteLandingList()
	client := proto.NewProtobufServiceClient(conn)
	resp, err := client.CountLandings(ctx, mll)
	if err != nil {
		t.Fatalf("CountLandings failed: %v", err)
	}
	require.Equal(t, int32(len(mll.MeteoriteLandings)), resp.Count)
}
