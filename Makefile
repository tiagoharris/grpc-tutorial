SHELL := /bin/bash

## help: show this help message
help:
	@ echo -e "Usage: make [target]\n"
	@ sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'

## protoc-all: download namely/protoc-all docker image
protoc-all:
	@ docker pull namely/protoc-all

## proto: compile protofiles
proto: protoc-all
	@ docker run -v `pwd`:/defs namely/protoc-all -d proto --go-source-relative -l go ; \
	echo "protofile was compiled"

## proto-ruby: compile protofiles to ruby
proto-ruby: protoc-all
	@ docker run -v `pwd`:/defs namely/protoc-all -d proto -l ruby ; \
	echo "protofile was compiled to Ruby"

## build-server: build grpc server's binary
build-server:
	@ cd server ; \
	go build -a -installsuffix cgo -o server .

## build-server-main: build main file that runs the grpc server
build-server-main:
	@ cd cmd/server ; \
	go build -a -installsuffix cgo -o main .

## run-server: run grpc server at port 4040
run-server: export TCP_PORT=4040
run-server: build-server build-server-main
	@ ./cmd/server/main

## build-client: build grpc client's binary
build-client:
	@ cd client ; \
	go build -a -installsuffix cgo -o client .

## build-client-main: build main file that runs the grpc client
build-client-main:
	@ cd cmd/client ; \
	go build -a -installsuffix cgo -o main .

## run-client: run grpc client that dials to port 4040
# if you need to enable a more verbosing log level to debug connection issues:
#run-client: export GRPC_GO_LOG_VERBOSITY_LEVEL=99
#run-client: export GRPC_GO_LOG_SEVERITY_LEVEL=info
run-client: export TCP_PORT=4040
run-client: build-client build-client-main
	@ ./cmd/client/main

## bundle: run bundler to install dependency defined at Gemfile
bundle:
	@ cd ruby ; \
	bundle

## run-ruby-client: run ruby grpc client that dials to port 4040
run-ruby-client: export TCP_PORT=4040
run-ruby-client:
	@ cd ruby ; \
	ruby client.rb

## test: run unit tests
test:
	@ go test -v ./...

## gen-certs: generate both certificate and private key files
gen-certs:
	@ openssl req -newkey rsa:2048 -nodes -keyout cert/localhost.key -out cert/localhost.csr -config cert/san/san.cnf; \
	openssl req -key cert/localhost.key -new -x509 -days 365 -out cert/localhost.crt -config cert/san/san.cnf