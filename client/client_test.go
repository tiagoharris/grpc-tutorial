package client

import (
	"context"
	"errors"
	"testing"

	proto "bitbucket.org/tiagoharris/grpc-tutorial/gen/pb-go"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
)

func TestNewClient(t *testing.T) {
	testCases := []struct {
		name          string
		certPath      string
		expectedError error
	}{
		{
			name:          "error loading cert",
			certPath:      "invalid",
			expectedError: errors.New("loading tls cert: open invalid: no such file or directory"),
		},
		{
			name:          "error dialing",
			certPath:      "../cert/localhost.crt",
			expectedError: errors.New("dialing: context deadline exceeded"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := NewClient("some port", tc.certPath, 0)
			require.NotNil(t, err)
			require.Equal(t, tc.expectedError.Error(), err.Error())
		})
	}
}

type MockedProtobufServiceClient struct {
	mock.Mock
}

func (m *MockedProtobufServiceClient) CountLandings(ctx context.Context, in *proto.MeteoriteLandingList, opts ...grpc.CallOption) (*proto.CountResponse, error) {
	var count *proto.CountResponse
	args := m.Called(ctx, in, opts)
	if args.Get(0) != nil {
		count = args.Get(0).(*proto.CountResponse)
	}
	return count, args.Error(1)
}

func TestCountLandings(t *testing.T) {
	testCases := []struct {
		name          string
		mockClosure   func(mocked *MockedProtobufServiceClient)
		expectedCount *proto.CountResponse
		expectedError error
	}{
		{
			name: "error",
			mockClosure: func(mocked *MockedProtobufServiceClient) {
				mocked.On("CountLandings", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error"))
			},
			expectedError: errors.New("calling 'client.CountLandings': error"),
		},
		{
			name: "happy path",
			mockClosure: func(mocked *MockedProtobufServiceClient) {
				count := &proto.CountResponse{Count: 2}
				mocked.On("CountLandings", mock.Anything, mock.Anything, mock.Anything).Return(count, nil)
			},
			expectedCount: &proto.CountResponse{Count: 2},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mockedProtobufServiceClient := new(MockedProtobufServiceClient)
			tc.mockClosure(mockedProtobufServiceClient)
			client := GRPClient{
				client: mockedProtobufServiceClient,
			}
			count, err := client.CountLandings(context.TODO(), nil)
			if tc.expectedError == nil {
				if err != nil {
					t.Fatalf("expected no error, got %v", err)
				}
			} else if err == nil {
				t.Fatal("expected error to occur")
			}
			if err != nil {
				require.Equal(t, tc.expectedError.Error(), err.Error())
			}
			if count != nil {
				require.Equal(t, count.Count, tc.expectedCount.Count)
			}
		})
	}
}
