package client

import (
	"context"
	"fmt"
	"time"

	proto "bitbucket.org/tiagoharris/grpc-tutorial/gen/pb-go"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Client defines CountLandings grpc service to be implemented.
type Client interface {
	CountLandings(ctx context.Context, mll *proto.MeteoriteLandingList, opts ...grpc.CallOption) (*proto.CountResponse, error)
}

// GRPClient implements the Client interface.
type GRPClient struct {
	addr   string
	client proto.ProtobufServiceClient
}

// NewClient returns GRPCClient.
// certPath is the path to the certificate file ('.crt').
func NewClient(addr, certPath string, timeout int) (Client, error) {
	creds, err := credentials.NewClientTLSFromFile(certPath, "")
	if err != nil {
		return nil, errors.Wrap(err, "loading tls cert")
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	defer cancel()
	conn, err := grpc.DialContext(ctx, fmt.Sprintf("localhost:%s", addr), grpc.WithBlock(), grpc.WithTransportCredentials(creds))
	if err != nil {
		return nil, errors.Wrap(err, "dialing")
	}
	return &GRPClient{
		addr:   addr,
		client: proto.NewProtobufServiceClient(conn),
	}, nil
}

// CountLandings invokes the correspondent grcp service to count the given *proto.MeteoriteLandingList.
func (gc *GRPClient) CountLandings(ctx context.Context, mll *proto.MeteoriteLandingList, opts ...grpc.CallOption) (*proto.CountResponse, error) {
	resp, err := gc.client.CountLandings(ctx, mll)
	if err != nil {
		return nil, errors.Wrap(err, "calling 'client.CountLandings'")
	}
	return resp, nil
}
