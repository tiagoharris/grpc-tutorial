package main

import (
	"context"
	"log"
	"os"

	"bitbucket.org/tiagoharris/grpc-tutorial/builder"
	"bitbucket.org/tiagoharris/grpc-tutorial/client"
	"github.com/pkg/errors"
)

func main() {
	ctx := context.Background()
	log := log.New(os.Stdout, "GRPC CLIENT : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	if err := run(ctx, log); err != nil {
		log.Println("main: error:", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, log *log.Logger) error {
	log.Println("main: Initializing GRPC client")
	defer log.Println("main: Completed")

	port := os.Getenv("TCP_PORT")
	if port == "" {
		return errors.New("main: specify grpc server port via environment variable PORT")
	}

	client, err := client.NewClient(port, "cert/localhost.crt", 30)
	if err != nil {
		return errors.Wrap(err, "initializing client")
	}

	resp, err := client.CountLandings(ctx, builder.BuildMeteoriteLandingList())
	if err != nil {
		return errors.Wrap(err, "calling client.CountLandings()")
	}

	log.Println("main: total landings: ", resp.Count)

	return nil
}
