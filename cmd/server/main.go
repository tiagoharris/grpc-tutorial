package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/tiagoharris/grpc-tutorial/server"
	"github.com/pkg/errors"
)

func run(log *log.Logger) error {
	log.Println("run: Initializing GRPC server")
	defer log.Println("run: Completed")

	// Get server port from env var
	port := os.Getenv("TCP_PORT")
	if port == "" {
		return errors.New("specify server's port via TCP_PORT env var")
	}

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	server, err := server.NewServer(port, "cert/localhost.crt", "cert/localhost.key")
	if err != nil {
		return errors.Wrap(err, "initializing server")
	}
	go func() {
		log.Printf("serve: GRPC server listening on %s", port)
		serverErrors <- server.Run()
	}()

	// Blocking main and waiting for shutdown.
	select {
	case err := <-serverErrors:
		return errors.Wrap(err, "server error")

	case sig := <-shutdown:
		log.Printf("serve: %v: Start shutdown", sig)
		server.Close()
	}
	return nil
}

func main() {
	log := log.New(os.Stdout, "GRPC SERVER : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	if err := run(log); err != nil {
		log.Println("main: error:", err)
		os.Exit(1)
	}
}
