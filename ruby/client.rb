this_dir = File.expand_path(File.expand_path('..'))
lib_dir = File.join(this_dir, 'gen/pb-ruby')
@cert_dir = File.join(this_dir, 'cert')
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

require 'grpc'
require 'nasa_meteorite_landings_services_pb'
require 'json'
require 'logger'
require 'google/protobuf/well_known_types'

include Proto

def build_payload
    mll = MeteoriteLandingList.new
    ml = MeteoriteLanding.new(
        id: "6", 
        name: "Abee", 
        fall: "Fell", 
        mass: "107000", 
        nametype: "Valid", 
        recclass: "EH4", 
        reclat: "54.216670", 
        reclong: "-113.000000",
        geolocation: GeoLocation.new(
            type: "Point",
            coordinates: [-113, 54.21667]
        ),
        year: Google::Protobuf::Timestamp.new({seconds: Time.new(1952).to_i})
    )
    ml2 = MeteoriteLanding.new(
        id: "10", 
        name: "Acapulco", 
        fall: "Fell", 
        mass: "1914", 
        nametype: "Valid", 
        recclass: "Acapulcoite", 
        reclat: "16.883330", 
        reclong: "-99.900000",
        geolocation: GeoLocation.new(
            type: "Point",
            coordinates: [-99.9, 16.88333]
        ),
        year: Google::Protobuf::Timestamp.new({seconds: Time.new(1976).to_i})
    )
    mll.MeteoriteLandings << ml
    mll.MeteoriteLandings << ml2
    mll
end

def main
    log = Logger.new(STDOUT)
    log.info("main: Initializing Ruby GRPC client")

    port = ENV["TCP_PORT"]
    if port.nil? or port.empty? then
        log.error("main: specify grpc server port via environment variable PORT")
        exit(1)
    end
    
    creds = GRPC::Core::ChannelCredentials.new(File.read("#{@cert_dir}/localhost.crt"))
    stub = Proto::ProtobufService::Stub.new("localhost:#{port}", creds)
    begin
        resp = stub.count_landings(build_payload)
        log.info("total landings: #{resp.count}")
    rescue
        log.error("#{$!.class}: #{$!.message}")
        $@.each { |loc| puts loc }
    ensure
        log.info("main: Completed")
        log.close
    end

end

main