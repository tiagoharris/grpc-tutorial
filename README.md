# grpc-tutorial

This tutorial shows how to implement a [GRPC](https://grpc.io/) server in [Golang](https://golang.org/), as well as how to define a [GRPC](https://grpc.io/) service and how to consume it from [Golang](https://golang.org/) and [Ruby](https://www.ruby-lang.org/en/) clients.

It uses [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) for authentication.

## What is being used

- [Docker](https://www.docker.com/)
- [docker-protoc](https://github.com/namely/docker-protoc), to compile proto files
- [OpenSSL](https://www.openssl.org/) to generate both certificate and private key files
- [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security)

## Running it

First, download the dependencies defined in [go.mod](./go.mod)

```
go mod tidy
```

Then, compile the proto file, for the [Golang](https://golang.org/) client

```
make proto
```

Now it's time to generate both certificate and private key files:

```
make gen-certs
```

Now, launch the GRCP server:

```
make run-server
```

### Golang client

```
make run-client
```

### Ruby client

First, compile the proto file for the [Ruby](https://www.ruby-lang.org/en/) client

```
make proto-ruby
```

Then

```
make run-ruby-client
```

## Avaliable targets in [Makefile](./Makefile)

```
make help
```